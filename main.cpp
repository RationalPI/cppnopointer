#include <memory>
#include <vector>

#include <iostream>

#if __cplusplus >= 201703L
#include <optional>
template<typename T>
using optional=std::optional<T>;
#else
//https://github.com/TartanLlama/optional/blob/master/include/tl/optional.hpp
template<typename T>
using opt=std::optional<T>;
#endif

struct I{
	virtual void aze()=0;
};

struct BaseClass: public I{
	virtual ~BaseClass()=default;
	std::vector<int> value;

	// I interface
public:
	void aze() override
	{
		std::cout << "aze" << std::endl;
	}
};

struct DefinedT: public BaseClass{};

struct OnlyDeclaredT;

struct Owner{
	//! ??? optional/notOptional
	//! ??? owner/reference
	//! ??? initialized/uninitialized
	//! defautlConstructible
	//! declared
	//! memory unsafe
	//! in heap memory
	OnlyDeclaredT* RAW_POINTER_OF_DEATH;

	//! notOptionalowner
	//! default initialized
	//! defautlConstructible
	//! defined
	//! in stack memory
	DefinedT valueT;

	//! notOptional
	//! reference (not reasignable)
	//! default initialized
	//! notDefautlConstructible
	//! defined
	DefinedT& refT;

	template<typename T>
	using ref=T&;


	//! optional,
	//! owner,
	//! default initialized empty,
	//! defautlConstructible,
	//! need only declared type,
	//! in heap memory
	template<typename T>
	using unique=std::unique_ptr<T>;

	unique<OnlyDeclaredT> uniqueT;

	//! optional,
	//! sharedOwner,
	//! default initialized empty,
	//! defautlConstructible,
	//! need only declared type,
	//! in heap memory
	template<typename T>
	using shared=std::shared_ptr<T>;

	shared<OnlyDeclaredT> sharedT;


	//! optional,
	//! reference (reasignable),
	//! default initialized empty,
	//! defautlConstructible,
	//! need only declared type
	template<typename T>
	using optRef=optional<std::reference_wrapper<T>>;
	optRef<OnlyDeclaredT> optRefT;




	ref<I> refi=valueT;
	unique<I> urefi=std::make_unique<DefinedT>();
	shared<I> srefi=std::make_shared<DefinedT>();
	optRef<I> orrefi=valueT;

	void polymotphisme(){
		refi.aze();
		urefi->aze();
		srefi->aze();
		orrefi->get().aze();
	}


	Owner(DefinedT& t):refT(t){}
	void dereferencing();
	void valueAssignation();
	void referenceReassignation();
	void clean(){}
	void upCast();
	void downCast();

};



struct OnlyDeclaredT:public DefinedT{
	using DefinedT::DefinedT;
};

void Owner::dereferencing(){
	auto use=[](DefinedT& t){};

	use(valueT);
	use(refT);
	if(uniqueT) use(*uniqueT);
	if(sharedT) use(*sharedT);
	if(optRefT) use(*optRefT);
}

void Owner::valueAssignation(){
	DefinedT dt;
	OnlyDeclaredT odt;

	valueT=dt;
	refT=dt;

	if(uniqueT) *uniqueT=odt;
	if(sharedT) *sharedT=odt;
	if(optRefT) *optRefT=odt;
}

void Owner::referenceReassignation(){
	static OnlyDeclaredT odt;

	optRefT=odt;
}

void Owner::upCast(){
	//Upcasting is a process of creating a pointer or a reference of the derived class object as a base class pointer.
	auto use=[](BaseClass& t){};

	use(valueT);
	use(refT);
	if(uniqueT) use(*uniqueT);
	if(sharedT) use(*sharedT);
	if(optRefT) use(*optRefT);
}

void Owner::downCast(){
	//Downcasting is an opposite process for upcasting. It converts base class pointer to derived class pointer. Downcasting must be done manually. It means that you have to specify explicit typecast.

	auto use=[](DefinedT& t){};

	auto upcast=[&](BaseClass& upcasted){

		if(auto defined=dynamic_cast<DefinedT*>(&upcasted)){
			use(*defined);
		}
	};

	upcast(valueT);
	upcast(refT);
	if(uniqueT) upcast(*uniqueT);
	if(sharedT) upcast(*sharedT);
	if(optRefT) upcast(*optRefT);
}



int main(){
	DefinedT t;
	Owner o(t);
	o.dereferencing();
	o.valueAssignation();
	o.upCast();

	o.polymotphisme();

	return 0;
}
